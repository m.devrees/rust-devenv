// use std::process::ExitCode;
extern crate dialoguer;
extern crate homedir;
extern crate whoami;

mod dialog;
mod service;

use dialog::handle_input;
use whoami::username;
// use homedir::get_my_home;

fn main() -> Result<(), std::io::Error> {
    let _username: String = username();
    let _username = handle_input("Enter your username".to_string(), _username);
    // let installpath = handle_input("Path to installdirectory".to_string(), get_my_home().unwrap().unwrap);
    Ok(())
}
