use dialoguer::{theme::ColorfulTheme, Input};

pub fn handle_input(question: String, initial_text: String) -> String {
    let input: String = Input::with_theme(&ColorfulTheme::default())
    .with_prompt(question)
    .with_initial_text(initial_text)
    .interact_text()
    .unwrap();
    input
}
